import sys, os
# add the demos directory into the Python's path
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../demos")

# run the demos
import tasse_avp
import tasse_qnavp
import tasse_drl
import tasse_drap
import tasse_cp
import tasse_cdr
import tasse_cdrl




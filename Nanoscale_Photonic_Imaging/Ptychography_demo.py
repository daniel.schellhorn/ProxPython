import sys, os
# add the demos directory into the Python's path
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../demos")

# run the demo
import Ptychography_PHeBIE_Wilke2
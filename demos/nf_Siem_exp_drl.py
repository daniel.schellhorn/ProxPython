
import SetProxPythonPath
from proxtoolbox.experiments.phase.Krueger_Experiment import Krueger_Experiment

Krueger = Krueger_Experiment(algorithm = 'DRl', data_ball = 20,
                             lambda_0 = 0.5, lambda_max = 0.5)
Krueger.run()
Krueger.show()

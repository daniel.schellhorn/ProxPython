
import SetProxPythonPath
from proxtoolbox.experiments.phase.Sparse2_Experiment import Sparse2_Experiment

Sparse2 = Sparse2_Experiment(algorithm='CP', debug=False)
Sparse2.run()
Sparse2.show()

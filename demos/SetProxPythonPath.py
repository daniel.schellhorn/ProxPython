#Sylvain

# Module to be imported by test modules to set the ProxPython path
# after this is called, can use import statements like this one:
# from proxtoolbox.experiments.phase import Phase, JWST_AltP_in

import sys
import os
# add the proxpython directory into the Python's path
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/..")


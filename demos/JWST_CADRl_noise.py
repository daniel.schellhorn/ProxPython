
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='CADRl', formulation='cyclic', 
                       lambda_0=1.0, lambda_max=1.0, MAXIT=6000, 
                       noise=True, rotate=True)
JWST.run()
JWST.show()

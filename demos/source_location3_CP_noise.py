
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='CP', formulation='cyclic', noise=True)
sourceExp.run()
sourceExp.show()

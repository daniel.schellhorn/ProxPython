
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment


CDP = CDP_Experiment(algorithm='CDRl', Nx=256, formulation='cyclic', 
                     lambda_0=0.3, lambda_max=0.3, MAXIT=1000, TOL=1e-8,
                     warmup_iter=50)
CDP.run()
CDP.show()

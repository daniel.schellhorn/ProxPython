
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

# Note: this demo has the same behavior as Matlab's with the same initial
# conditions. The algorithm does not converge.

JWST = JWST_Experiment(algorithm='Wirtinger')
JWST.run()
JWST.show()

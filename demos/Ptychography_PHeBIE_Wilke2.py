
import SetProxPythonPath
from proxtoolbox.experiments.ptychography.ptychographyExperiment import PtychographyExperiment

warmupParams = {
        'algorithm': 'DRl',
        'MAXIT': 10,
        'TOL': 5e-5,
        'lambda_0': 0.75,
        'lambda_max': 0.75,
        'lambda_switch': 20,
        'data_ball': 1e-30,
        'diagnostic': True,
        'rotate': False,
        'iterate_monitor_name': 'IterateMonitor',
        'verbose': 1,
        'graphics': 0,
        'anim': False
    }

Pty = PtychographyExperiment(warmup=True, MAXIT=10, warmup_params=warmupParams)
Pty.run()
Pty.show()

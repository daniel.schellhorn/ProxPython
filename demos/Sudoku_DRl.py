
import SetProxPythonPath
from proxtoolbox.experiments.sudoku.sudokuExperiment import SudokuExperiment

# if a puzzle is not specified, the sudoku experiment uses the
# default puzzle, which is the same as the one below:
puzzle = ((2,0,0,0,0,1,0,3,0),
         (4,0,0,0,8,6,1,0,0),
         (0,0,0,0,0,0,0,0,0),
         (0,0,0,0,1,0,0,0,0),
         (0,0,0,0,0,0,9,0,0),
         (0,0,5,0,0,3,0,0,7),
         (0,0,0,0,0,0,0,0,0),
         (1,0,0,0,0,7,4,9,0),
         (0,2,4,1,0,0,0,0,0))

Sudoku = SudokuExperiment(algorithm='DRl', sudoku=puzzle)
Sudoku.run()
Sudoku.show()

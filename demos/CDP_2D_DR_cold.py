
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment


CDP = CDP_Experiment(algorithm='DRl', Nx=256, lambda_0=1.0, lambda_max=1.0,
                     MAXIT=6000, TOL=1e-8)
CDP.run()
CDP.show()

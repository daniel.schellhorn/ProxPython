
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='AvP2', formulation='NSLS', lambda_0=0.3, lambda_max=0.3)
sourceExp.run()
sourceExp.show()

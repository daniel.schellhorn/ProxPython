
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='CP', formulation='cyclic', MAXIT=1000,
                       TOL=5e-15, rotate=True)
JWST.run()
JWST.show()

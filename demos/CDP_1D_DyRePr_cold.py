
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment

# Note: This demo does not converge (it blows up, same with ProxMatlab)

CDP = CDP_Experiment(algorithm='DyRePr', constraint='support only')
CDP.run()
CDP.show()

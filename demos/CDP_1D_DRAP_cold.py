
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment

CDP = CDP_Experiment(algorithm='DRAP')
CDP.run()
CDP.show()

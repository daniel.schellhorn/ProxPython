
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment


CDP = CDP_Experiment(algorithm='DRl', lambda_0=0.60, lambda_max=0.60,
                     MAXIT=1000)
CDP.run()
CDP.show()


import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='ADMM', lambda_0=3.0, lambda_max=3.0,
                       MAXIT=300, noise = True, rotate = True)
JWST.run()
JWST.show()

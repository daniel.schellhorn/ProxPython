# ProxToolbox

The ProxToolbox is a collection of modules for solving mathematical problems
using fixed point iterations with proximal operators. It was used to generate
many if not all of the numerical experiments conducted in the papers in the
ProxMatlab Literature folder.
For a complete listing of papers with links go to
http://vaopt.math.uni-goettingen.de/en/publications.php.

This site is maintained by the Working Group in Variational Analysis of the 
Institute for Numerical and Applied Mathematics at the University of Göttingen.


Additional binary data

    Phase (28mb):  http://vaopt.math.uni-goettingen.de/data/Phase.tar.gz
    Ptychography (417mb): http://vaopt.math.uni-goettingen.de/data/Ptychography.tar.gz
    CT (1.7mb):  http://vaopt.math.uni-goettingen.de/data/CT.tar.gz

The binary data need to be unpacked in the InputData subdirectory source code. 
For example, the Phase data in ProxPython would be in ProxPython/InputData/Phase.

#         Acknowledgements

    Main Author: Russell Luke, University of Göttingen

    Contributors:

    Sylvain Gretchko, Inst. for Numerical and Applied Math, Universität Göttingen (python),  
	Matthijs Jansen, I. Institute for Physics, University of Göttingen (Orbital Tomography); 
	Alexander Dornheim, Inst. for Numerical and Applied Math, Universität Göttingen (python); 
	Stefan Ziehe, Inst. for Numerical and Applied Math, Universität Göttingen (python); 
	Rebecca Nahme, Inst. for Numerical and Applied Math, Universität Göttingen (python); 
	Matthew Tam, CARMA, University of Newcastle, Australia (Ptychography); 
	Pär Mattson, Inst. for Numerical and Applied Math, Universität Göttingen (Ptychography); 
	Robin Wilke, Inst. for X-Ray Physics, Univesität Göttingen (Ptychography and Phase); 
	Robert Hesse, Inst. for Numerical and Applied Math, Universität Göttingen; 
	Alexander Stalljahn, Inst. for Numerical and Applied Math, Universität Göttingen (Sudoku).

    Funding:

        This has grown over the years and has been supported in part by:

        NASA grant NGT5-66; the Pacific Institute for Mathematical Sciences (PIMS); 
	    USA NSF Grant DMS-0712796; 
	    German DFG grant SFB-755 TPC2; 
	    German Israeli Foundation (GIF) grant G-1253-304.6/2014.


*********************************

#         Getting started      

*********************************

The easiest way to use ProxPython is to run some of the included demos.
There are different families of experiments and various demos.
All the demos are located in the ProxPython/demos folder. For convenience,
all the Nanoscale Photonic Imaging demos have been grouped together
in the ProxPython/Nanoscale_Photonic_Imaging folder.

To run a demo, open a command line. Assuming ProxPython is in your current folder,
change your current folder to demos by
    
    cd ProxPython/demos

Then just type
    
    python3 demo_name

to run the demo called "demo_name".

For example,

    python3 JWST_RAAR.py

will run the RAAR algortihm on the JWST experiment.


*********************************

#        Structure                

*********************************

What is the basic structure of ProxPython?

I. proxtoolbox folder

This is the heart of the proxtoolbox. Most of the code is found here. 
There are four subfolders:

1. experiments

    This folder contains the different experiments to which the 
    proxtoolbox can be applied. It defines the abstract Experiment
    class which contains all the information that describes a given
    experiment. Essentially, this class loads or generates the data
    used in the experiment and instanciates the algorithm that works on
    this data. This class is meant to be derived from to implement
    concrete experiments.
    There are currently four families of experiments: the phase retrieval
    experiments, computed tomography (CT), ptychography, and Sudoku. 
    The orbital tomography experiment will be integrated soon.

2. algorithms

    The proxtoolbox allows the use of different algortihms. 
    This folder contains the abstract Algorithm class and
    various concrete classes. 
    At moment the following algortihms have been implemented:

     - AP:   Alternating Projections
     - AvP:  Averaged Projections
     - CDRl: relaxed version of a cyclic averaged 
             alternating reflections method (CAAR) proposed by
             Borwein and Tam, Journal of Nonlinear and Convex Analysis
             Volume 16, Number 4.  The relaxation is the relaxed
             Douglas Rachford algorithm proposed and analysed by Luke,
             Inverse Problems, 2005 and SIAM J. on Optimization, 2008)
     - CP:   Cyclic Projections
     - DRAP: a type of hybrid Douglas-Rachford and Alternating projections 
             proposed by Nguyen H. Thao, ``A convergent relaxation of the 
             Douglas--Rachford algorithm", Comput. Optim. Appl., 70
             (2018), pp. 841--863.. 
     - DRl:  Douglas-Rachford-lambda (same as RAAR below)
     - HPR:  (Heinz-Patrick-Russell algorithm.  
             See Bauschke,Combettes&Luke, Journal of the Optical 
             Society of America A, 20(6):1025-1034 (2003))
     - PHeBIE: Proximal Heterogenious Block Implicit-Explicit (PHeBIE) 
             minimzation algorithm as proposed in the paper
             "Proximal Heterogeneous Block Implicit-Explicit Method and
             Application to Blind Ptychographic Diffraction Imaging",
             R. Hesse, D. R. Luke, S. Sabach and M. K. Tam, 
             SIAM J. on Imaging Sciences, 8(1):426--457 (2015))
     - QNAvP:Quasi-Newton Accelerated Averaged Projections
     - RAAR: Relaxed Averaged Alternating Reflection algorithm. 
             For background see:
             D.R.Luke, Inverse Problems 21:37-50(2005)
             D.R. Luke, SIAM J. Opt. 19(2): 714--739 (2008))



3. proxoperators
    Contains the prox operators classes used by the algorithms.


4. utils
    Contains some utilities, mostly for loading and processing data.

*********************************
#         II. InputData          
*********************************

When you first download the proxtoolbox this folder should be empty.
When you call a ART/Phase/Ptychography demo the first time, you are asked if you
want to automatically download the InputData for ART, phase or ptychography.
If this does not work you need to download the data manually from:

    CT (1.7mb):  http://vaopt.math.uni-goettingen.de/data/CT.tar.gz

    Phase (28mb):  http://vaopt.math.uni-goettingen.de/data/Phase.tar.gz

    Ptychography (417mb): http://vaopt.math.uni-goettingen.de/data/Ptychography.tar.gz

Extract the data and store it in
InputData/CT/
InputData/Phase/
InputData/Ptychography/

*********************************
#         III. Demos            
********************************

All the demos are located in the demos folder. For convenience,
all the Nanoscale Photonic Imaging demos have been grouped together
in the ProxPython/Nanoscale_Photonic_Imaging folder. To run a demo, 
change your current folder to demos or Nanoscale_Photonic_Imaging and 
type

    python3 demo_name

Depending on your operating system, you may have to type the following:

    py demo_name


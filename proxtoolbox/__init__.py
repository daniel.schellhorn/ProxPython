# -*- coding: utf-8 -*-

"""
  ProxToolbox
  ~~~~~~~~~~~

  ..

  :Copyright: 2015, `D. Russell Luke <http://num.math.uni-goettingen.de/~r.luke/>`_
  :License: BSD, see LICENSE for more details.
  :Contributors:

    * Sylvain Gretchko, Inst. for Numerical and Applied Math, Universität Göttingen (python),  
    * Matthijs Jansen, I. Institute for Physics, University of Göttingen (Orbital Tomography); 
    * Alexander Dornheim, Inst. for Numerical and Applied Math, Universität Göttingen (python); 
    * Stefan Ziehe, Inst. for Numerical and Applied Math, Universität Göttingen (python); 
    * Rebecca Nahme, Inst. for Numerical and Applied Math, Universität Göttingen (python); 
    * Matthew Tam, CARMA, University of Newcastle, Australia (Ptychography); 
    * Pär Mattson, Inst. for Numerical and Applied Math, Universität Göttingen (Ptychography); 
    * Robin Wilke, Inst. for X-Ray Physics, Univesität Göttingen (Ptychography and Phase); 
    * Robert Hesse, Inst. for Numerical and Applied Math, Universität Göttingen; 
    * Alexander Stalljahn, Inst. for Numerical and Applied Math, Universität Göttingen (Sudoku).

  :Funding:	 This has grown over the years and has been supported in part by:

       * NASA grant NGT5-66
       * Pacific Institute for Mathematical Sciences (PIMS)
       * USA NSF Grant DMS-0712796
       * German DFG grant SFB-755 TPC2
       * German Israeli Foundation (GIF) grant G-1253-304.6/2014.

"""

from .algorithms import *
from .experiments import *
from .proxoperators import *
from .utils import *

__all__ = ["algorithms",
           "experiments",
           "proxoperators",
           "utils"]

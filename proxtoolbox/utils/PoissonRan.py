# Poisson random number generation with mean of xm
# April 6, 2005 Ning Lei
# see numerical recipe C++ version
# ref: y(i+1) = xm^i*exp(-xm)/factorial(i); see Leo Breiman Probability

from numpy.random import rand
from numpy import exp, sqrt, log, tan, pi, floor, zeros, ones
from scipy.special import gammaln
import numpy as np

def PoissonRan(xm):
    """
    Poisson random number generation with mean of xm

    See numerical recipe C++ version
    ref: y(i+1) = xm^i*exp(-xm)/factorial(i); see Leo Breiman Probability
        
    Parameters
    ----------
    xm : real number
        Mean
        
    Returns
    -------
    em : real number
        Sample from the parameterized Poisson distribution

    Notes    
    -----
    This code suffers from numerical precision issues. With big 
    values for xm (for example, xm = 6.910459246361838e+27, as was 
    initially the case with the JWST experiment) the difference
    with Matlab is substential. In Python, one exponential 
    evalutation gives +inf while it gives zero in Maltab.
    """

    oldm = -1

    if xm<12:
        if xm != oldm:
            oldm = xm
            g = exp(-xm)

        em = -1
        t = 1

        em = em+1
        t = t*rand()
        while t>g:
            em = em+1
            t = t*rand()
    else:
        if xm != oldm:
            oldm = xm
            sq = sqrt(2.0*xm)
            alxm = log(xm)
            g = xm*alxm-gammaln(xm+1)

        y = tan(pi*rand())
        em = sq*y+xm

        while em < 0:
            y = tan(pi*rand())
            em = sq*y+xm
  
        em = floor(em)

        t = 0.9*(1+y*y)*exp(em*alxm-gammaln(em+1)-g)

        while rand() > t:
            y = tan(pi*rand())
            em = sq*y+xm

            while em < 0:
                y = tan(pi*rand())
                em = sq*y+xm

            em = floor(em)
            t = 0.9*(1+y*y)*exp(em*alxm-gammaln(em+1)-g)
   
    return em


from proxtoolbox.algorithms.RAAR import RAAR
from proxtoolbox.utils.cell import Cell, isCell
from numpy import exp


class DRl(RAAR):
    """
    Relaxed Averaged Alternating Reflection algorithm.
    
    For background see:
    D.R.Luke, Inverse Problems 21:37-50(2005)
    D.R. Luke, SIAM J. Opt. 19(2): 714--739 (2008).
    D. R. Luke and A.-L. Martins, "Convergence Analysis 
    of the Relaxed Douglas Rachford Algorithm" , 
    SIAM J. on Optimization, 30(1):542--584(2020).
    https://epubs.siam.org/doi/abs/10.1137/18M1229638


    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Aug 18, 2017.    
    """
# Same code as RAAR algorithm

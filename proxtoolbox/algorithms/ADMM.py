
from proxtoolbox.algorithms.algorithm import Algorithm
from proxtoolbox import algorithms
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
import numpy as np
from numpy import pi, zeros, conj
from numpy.fft import fft2, ifft2, fft, ifft


class ADMM(Algorithm):
    """
    Alternating directions method of multipliers for solving problems of the form :
    minimize f(x) + g(y), 
    subject to Ax=y

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on October 2, 2017.    
    """

    def __init__(self, experiment, iterateMonitor, accelerator):
        super(ADMM, self).__init__(experiment, iterateMonitor, accelerator)
        # instantiate Lagrange multiplier update object
        if hasattr(experiment, 'lagrange_mult_update'):
            lagrange_mult_update_name = experiment.lagrange_mult_update
            lagrange_mult_update_class = getattr(algorithms, lagrange_mult_update_name)
            self.lagrange_mult_update = lagrange_mult_update_class(experiment)
        else:
            raise AttributeError('Lagrange multiplier update not defined')


    def evaluate(self, u):
        """
        Update of the ADMM algorithm
       
        Parameters
        ----------
        u : a 3-dimensional cell
            The current iterate. 3 blocks of variables, primal (x),
            auxilliary (y) and dual variables corresponding to Lagrange 
            multipliers for the constraint Ax=y
               
        Returns
        -------
        u_new : a 3-dimensional cell
            The new iterate (same type as input parameter `u`).
        """

        lmbda = self.computeRelaxation()
        u_new = u.copy()  # this is only a shallow copy, but this will be enough in this case
        self.prox1.lmbda = lmbda
        u_new[0] = self.prox1.eval(u)
        self.prox2.lmbda = lmbda
        u_new[1] = self.prox2.eval(u_new)
        u_new[2] = self.lagrange_mult_update.eval(u_new)

        return u_new



    def getDescription(self):
        return self.getDescriptionHelper("\\lambda", self.lambda_0, self.lambda_max)

from proxtoolbox.algorithms.iterateMonitor import IterateMonitor
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.accessors import accessors
import numpy as np
from numpy import zeros, angle, trace, exp, sqrt, sum, matmul, array, reshape
from numpy.linalg import norm

class AvP2_IterateMonitor(IterateMonitor):
    """
    Algorithm analyzer for monitoring iterates of 
    projection algorithms for the PHeBIE algorithm. 
    Specialization of the IterateMonitor class.
    """

    def __init__(self, experiment):
        super(AvP2_IterateMonitor, self).__init__(experiment)
        self.gaps = None
        self.rel_errors = None
        self.product_space_dimension = experiment.product_space_dimension
        self.Nz = experiment.Nz
        name = experiment.experiment_name
        self.useExpForRelError = (name == 'CDP' or name == 'JWST' or name == 'Phase')


    def preprocess(self, alg):
        """
        Allocate data structures needed to collect 
        statistics. Called before running the algorithm.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm to be monitored.
        """
        super(AvP2_IterateMonitor, self).preprocess(alg)

        # In PHeBIE, the iterate u is a cell of blocks of variables.
        # In the analysis of Hesse, Luke, Sabach&Tam, SIAM J. Imaging Sciences, 2015
        # all blocks are monitored for convergence.
        self.u_monitor = self.u0[1].copy()

        # set up diagnostic arrays
        if self.diagnostic:
            self.gaps = self.changes.copy()
            self.gaps[0] = 999
            if self.truth is not None:
                self.rel_errors = self.changes.copy()
                self.rel_errors[0] = sqrt(999)
           
    def updateStatistics(self, alg):
        """
        Update statistics. Called at each iteration
        while the algorithm is running.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm being monitored.
        """
        # Here we monitor u[1] which has the shape of the data (no product space dimension)

        u = alg.u_new
        prev_u1 = self.u_monitor
        normM = self.norm_data
        
        tmp_change = sum(abs(u[1] - prev_u1)**2)/normM**2

        self.changes[alg.iter] = sqrt(tmp_change)
        if self.diagnostic:
            self.gaps[alg.iter] = self.calculateObjective(alg)
            if self.truth is not None:
                u_tmp = u[1]
                if u_tmp.ndim == 1:
                    u_tmp = reshape(u_tmp, (len(u_tmp),1))  # we want a true matrix not just a vector
                if self.useExpForRelError:
                    rel_error = norm(self.truth - exp(-1j*angle(trace(matmul(self.truth.T.conj(), u_tmp)))) * u_tmp) / self.norm_truth
                else:
                    rel_error = norm(self.truth - u_tmp)
                self.rel_errors[alg.iter] = rel_error

        self.u_monitor = u[1].copy()

    def calculateObjective(self, alg):
        # TODO Matlab code:
        
        # There is an objective function behind this method.  This has been
        # implemented for source localization, but not yet for phaes retrieval
        #    if strcmp(method_input.problem_family, 'Source_localization')
        #        method_input.gap(iter) = feval('NSLS_sourceloc_objective', u{3}, method_input); 
        #    else
        #
        #        method_input.gap(iter) = 999;  % just a dummy value.

        if self.optimality_monitor is not None:
            return self.optimality_monitor.calculateObjective(alg)
        else:
            # for now use dummy value
            objValue = 999
            return objValue

    def postprocess(self, alg, output):
        """
        Called after the algorithm stops. Store statistics in
        the given 'output' dictionary

        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that was monitored.
        output : dictionary
            Contains the last iterate and various statistics that
            were collected while the algorithm was running.
                
        Returns
        -------
        output : dictionary into which the following entries are added
            (when diagnostics are required)
        gaps : ndarray
            Squared gap distance normalized by the magnitude
            constraint
        """
        output = super(AvP2_IterateMonitor, self).postprocess(alg, output)
        if self.diagnostic:
            stats = output['stats']
            stats['gaps'] = self.gaps[1:alg.iter+1]
            if self.truth is not None:
                stats['rel_errors'] = self.rel_errors[1:alg.iter+1]
        return output
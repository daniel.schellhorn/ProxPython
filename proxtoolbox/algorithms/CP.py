from proxtoolbox.algorithms.algorithm import Algorithm
from numpy import exp


class CP(Algorithm):
    """
    Cyclic Projections algorithm 
    """

    def evaluate(self, u):
        """
        Update for Cyclic Projections algorithm.

        Parameters
        ----------
        u : ndarray 
            The current iterate.
        Returns
        -------
        u_new : ndarray
            The new iterate.
        """

        u_new = u
        nProx = len(self.proxOperators)
        for j in range(nProx):
            u_new = self.proxOperators[j].eval(u_new, j)
        return u_new



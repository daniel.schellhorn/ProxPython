
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
import numpy as np
from numpy.linalg import norm


class NSLS_sourceloc_objective:
    """
    objective function for the NSLS (NonSmooth Least Squares) formulation of the 
    source localization experiment
       
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Sept. 12, 2017.    
    """

    def __init__(self, experiment):
        self.sensors = experiment.sensors
        self.norm_data = experiment.norm_data
        self.data = experiment.data
        self.shift_data = experiment.shift_data

    def calculateObjective(self, alg):
        """
        Evaluate the objective function for the NSLS 
            
        Parameters
        ----------
        alg : algorithm instance
            The algorithm that is running
         
        Returns
        -------
        objValue : real
            The value of the objective function 
        """
        objValue = 0
        u = alg.u_new[2]
        if isCell(u):
            for j in range (len(u)):
                norm_vec = norm(self.shift_data[j] - u[j])
                objValue += (norm_vec - self.data[j])**2
        else:
            errMsg = "Only cells are supported for now"
            raise NotImplementedError(errMsg)

        return objValue




from  proxtoolbox.algorithms.algorithm import Algorithm

class AP(Algorithm):
    """
    Alternating Projection algorithm
    """

    def evaluate(self, u):
        """
        Update for Alternating projection algorithm. 

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The new iterate (same type as input parameter `u`).
        """

        tmp_u = self.proxOperators[1].eval(u)
        unew = self.proxOperators[0].eval(tmp_u)
        return unew


from proxtoolbox.algorithms.AvP import AvP
from proxtoolbox import proxoperators
from proxtoolbox.proxoperators.P_diag import P_diag
from proxtoolbox.proxoperators.Prox_product_space import Prox_product_space
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from numpy import mod, zeros
from numpy.linalg import norm
import copy


from numpy import exp

class DyRePr(AvP):
    """
    Dynamically Reweighted Prox mappings as presented in 
    Luke,Burke,Lyon "OPTICAL WAVEFRONT RECONSTRUCTION: THEORY AND
    NUMERICAL METHODS", SIAM Review, 2002, where it is 
    described as extended least squares:  a gradient 
    descent method for minimizing the log likelihood
    objective of the sum of squared distances to sets.  

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Aug.18, 2017.    
    """
    
    def __init__(self, experiment, iterateMonitor, accelerator):
        super(DyRePr, self).__init__(experiment, iterateMonitor, accelerator)
        

    def evaluate(self, u):
        """
        Update of the Averaged Projections algorithm
       
        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The new iterate (same type as input parameter `u`).

        Notes
        -----
        We try to figure out how the user is calling this.  Either
        (1) the user knows that averaged projections is alternating 
        projectons on the product space with one of the sets being 
        the diagonal, or (2) the user hasn't preprocessed this 
        and has just passed the list of prox mappings and left it to us 
        to rearrange and average.  If it is case (1) one of the 
        prox mappings will be P_Diag, otherwise we will assume
        that we are in case (2)
        """

        if self.has_p_diag is False:
            # Case (2):  the user hasn't preprocessed this 
            # and has just passed the list of prox mappings 
            # and left it to us to rearrange and average.
            u_intermed = Cell(self.n_product_Prox)
            for j in range(self.n_product_Prox):
                u_intermed[j] = u
            u_intermed = self.p_product_space.eval(u_intermed)        
            if self.accelerator is not None:
                u_intermed = self.accelerator.evaluate(u_intermed, self)
            for j in range(self.n_product_Prox):
                weight = norm(u_intermed[j], ord=2)**2
                u_intermed[j] /= weight + 1
            u_new = self.p_avg.eval(u_intermed)
        else:
            # Case (1) the user knows that averaged projections is alternating 
            # projectons on the product space with one of the sets being 
            # the diagonal.
            # First, evaluate the other prox op
            u_tmp = self.proxOperators[self.other_prox_index].eval(u)
            for j in range(self.n_product_Prox):
                if isCell(u):
                    weight = norm(u[j]-u_tmp[j], ord=2)**2
                    u_tmp[j] /= weight + 1
                else:
                    m, n, _p, _q = size_matlab(u)
                    if m == self.n_product_Prox:
                        weight = norm(u[j,:]-u_tmp[j,:], ord=2)**2
                        u_tmp[j,:] /= weight + 1
                    elif n == self.n_product_Prox:
                        weight = norm(u[:,j]-u_tmp[:,j], ord=2)**2
                        u_tmp[:,j] /= weight + 1
                    else:
                        weight = norm(u[:,:,j]-u_tmp[:,:,j], ord=2)**2
                        u_tmp[:,:,j] /= weight + 1

            # Finally, evaluate diag prox
            u_new = self.proxOperators[self.p_diag_index].eval(u_tmp)

        return u_new



from proxtoolbox.algorithms.algorithm import Algorithm
from proxtoolbox.algorithms.AvP import AvP 
from proxtoolbox.algorithms.QNAccelerator import QNAccelerator
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.cell import Cell, isCell
from numpy import zeros, ones, zeros_like
import numpy as np


class QNAvP(AvP):
    """
    Quasi-Newton Accelerated averaged projections algorithm

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Feb.18, 2018.    
    """
 
    def __init__(self, experiment, iterateMonitor, accelerator):
        super(QNAvP, self).__init__(experiment, iterateMonitor, accelerator)
        if accelerator is None:
            self.accelerator = QNAccelerator(experiment)

      
    def evaluate(self, u):
        """
        Update of the Quasi-Newton Accelerated
        averaged projections algorithm
       
        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The new iterate (same type as input parameter `u`).

        Notes
        -----
        We try to figure out how the user is calling this.  Either
        (1) the user knows that averaged projections is alternating 
        projectons on the product space with one of the sets being 
        the diagonal, or (2) the user hasn't preprocessed this 
        and has just passed the list of prox mappings and left it to us 
        to rearrange and average.  If it is case (1) one of the 
        prox mappings will be P_Diag, otherwise we will assume
        that we are in case (2)

        Code is similar to the AvP algorithm, but for some reason
        the use of the accelerator is a bit different. So for now
        we don't reuse the AvP code, except in the constructor.
        """

        if self.has_p_diag is False:
            # Case (2):  the user hasn't preprocessed this 
            # and has just passed the list of prox mappings 
            # and left it to us to rearrange and average.
            u_intermed = Cell(self.n_product_Prox)
            for j in range(self.n_product_Prox):
                u_intermed[j] = u
            u_intermed = self.p_product_space.eval(u_intermed)        
            #if self.accelerator is not None:
            #    u_intermed = self.accelerator.eval(u_intermed)
            u_new = self.p_avg.eval(u_intermed)
    
            # pass to samsara for quasi-Newton acceleration
            u_new = self.accelerator.evaluate(u_new, self)

        else: 
            # Case (1) the user knows that averaged projections is alternating 
            # projectons on the product space with one of the sets being 
            # the diagonal.
            # First, evaluate the other prox op
            u = self.proxOperators[self.other_prox_index].eval(u)
            #if self.accelerator is not None:
            #    u = self.accelerator.eval(u)
            # evaluate diag prox
            u_new = self.proxOperators[self.p_diag_index].eval(u)
            
            # Quasi-Newton acceleration:  since the diagonal
            # has redundant copies, we only need to accelerate one of
            # these, and then fill in the rest. How this is done
            # depends on the structure of u:
            if isCell(u_new):
                u_new[0] = self.accelerator.evaluate(u_new[0], self)
                for j in range(1, len(u_new)):
                    u_new[j] = u_new[0]
            else:
                K = self.n_product_Prox
                m, n, p, q = size_matlab(u_new)
                if m == K:
                    tmp = self.accelerator.evaluate(u_new[0,:].T, self)
                    if tmp.ndim == 1:
                        tmp = np.reshape(tmp, (len(tmp),1))  # we want a true matrix not just a vector
                    u_new = ones((K, 1)) @ tmp.T
                elif n == K:
                    tmp = self.accelerator.evaluate(u_new[:,0], self)
                    if tmp.ndim == 1:
                        tmp = np.reshape(tmp, (len(tmp),1))  # we want a true matrix not just a vector
                    u_new = tmp @ ones((1, K))
                elif p == K:
                    tmp = self.accelerator.evaluate(u_new[:,:,0], self)
                    for k in range(K):
                        u_new[:, :, k] = tmp
                elif q == K:
                    tmp = self.accelerator.evaluate(u_new[:,:,:,0], self)
                    for k in range(K):
                        u_new[:, :, :, k] = tmp
        return u_new

from proxtoolbox.algorithms.algorithm import Algorithm
from proxtoolbox.utils.cell import Cell, isCell
from numpy import exp


class CADRl(Algorithm):
    """
    Relaxed version of a cyclic averaged 
    alternating reflections method (CAAR) proposed by
    Borwein and Tam, Journal of Nonlinear and Convex Analysis
    Volume 16, Number 4.  The relaxation is the relaxed
    Douglas Rachford algorithm proposed and analysed by Luke,
    Inverse Problems, 2005 and SIAM J. on Optimization, 2008)    
    
    For more on the CDRl algorithm in the convex setting see:
    D. R. Luke, A. Martins and M. K. Tam. "Relaxed Cyclic Douglas-Rachford 
    Algorithms for Nonconvex Optimization." ICML 2018 Workshop: 
    Modern Trends in Nonconvex Optimization for Machine Learning, 
    Stockholm, July 2018.
    https://sites.google.com/view/icml2018nonconvex/papers
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Aug 18, 2017.    
    """

    def evaluate(self, u):
        """
        Update for the relaxed version of a cyclic averaged 
        alternating reflections algorithm 

        Parameters
        ----------
        u : ndarray 
            The current iterate.
               
        Returns
        -------
        u_new : ndarray
            The new iterate (same type as input parameter `u`).
        """
        lmbd = self.computeRelaxation()
        tmp_u = u
        nProx = len(self.proxOperators)
        for j in range(nProx-1):
            tmp1 = 2*self.proxOperators[j+1].eval(tmp_u, j+1) - tmp_u # reflection
            tmp2 = self.proxOperators[0].eval(tmp1, 0) # projection of the reflection
            tmp_u = (lmbd*(2*tmp2 - tmp1) + (1 - lmbd)*tmp1 + tmp_u)/2
        return tmp_u


    def getDescription(self):
        return self.getDescriptionHelper("\\lambda", self.lambda_0, self.lambda_max)

from  proxtoolbox.algorithms.algorithm import Algorithm
#from proxtoolbox.utils import fft, ifft
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.accessors import accessors
import numpy as np
from numpy import sqrt, conj, tile, mean, exp, angle, trace, reshape
from numpy.linalg import norm
from numpy.fft import fft2, ifft2, fft, ifft


class Wirtinger(Algorithm):
    """
    Wirtinger flow algorithm as implemented 
    by E. J. Candes, X. Li, and M. Soltanolkotabi
    "Phase Retrieval via Wirtinger Flow: Theory and Algorithms" 
    adapted for ProxToolbox

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on June 29, 2017.    
    """

    def __init__(self, experiment, iterateMonitor, accelerator):
        super(Wirtinger, self).__init__(experiment, iterateMonitor, accelerator)
        self.masks = experiment.masks
        if hasattr(experiment, 'masks_conj'):
            self.masks_conj = experiment.masks_conj
        else:
            self.masks_conj = conj(self.masks)
        self.data_sq = experiment.data_sq

        self.tau0 = 330
        self.mu = lambda t: min(1-exp(-t/self.tau0), 0.4)

    def evaluate(self, u):
        """
        Update for Wirtinger algorithm. 

        Parameters
        ----------
        u : ndarray 
            The current iterate.
               
        Returns
        -------
        u_new : ndarray
            The new iterate (same type as input parameter `u`).
        """
        L = self.product_space_dimension
        get, set, data_shape = accessors(u, self.Nx, self.Ny, L)

        if isCell(u):
            u_new = Cell(L)
            self.step = Cell(L)
        else:
            u_new = np.empty(u.shape, dtype=np.complex128)
            self.step = np.empty(u.shape, dtype=np.complex128)

        grad = np.zeros(data_shape, dtype=np.complex128)
        for j in range(L):
            grad += self.evaluateGradHelper(get(u, j), get(self.masks, j), get(self.masks_conj, j), get(self.data_sq, j))
        
        grad /= L
        step = self.mu(self.iter+1)/self.norm_data**2 * grad
            
        for j in range(L):
            set(self.step, j, step)
            set(u_new, j, get(u, j)-step)

        return u_new


    def evaluateGradHelper(self, u, masks, masks_conj, data_sq):
        m, n, _p, _q = size_matlab(u)
        if m > 1 and n > 1:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)
        else:
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)

        Bz = FFT(masks * u)
        C = (abs(Bz)**2 - data_sq) * Bz
        grad  = masks_conj * IFFT(C)
        return grad




# -*- coding: utf-8 -*-
"""
This package contains the different experiments to which the 
proxtoolbox can be applied. It defines the abstract Experiment
class which contains all the information that describes a given
experiment. Concrete experiment classes are derived from this 
class.
There are currently four families of experiments: the phase retrieval
experiments, computed tomography (CT), ptychography, and Sudoku. 
The orbital tomography experiment will be integrated soon.
"""

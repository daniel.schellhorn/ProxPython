from skimage.io import imread
from scipy.ndimage import binary_dilation
import numpy as np
import matplotlib.pyplot as plt

from proxtoolbox.experiments.orbitaltomography.orbitExperiment import OrbitalTomographyExperiment
from proxtoolbox.utils.visualization.stack_viewer import XYZStackViewer
from proxtoolbox.utils.OrbitalTomog import shifted_fft


class Molecule3D(OrbitalTomographyExperiment):
    @staticmethod
    def getDefaultParameters():  # TODO: update with good values for 3DOT
        defaultParams = {
            'experiment_name': 'CDI',
            'object': 'nonnegative',
            'constraint': 'nonnegative and support',
            'Nx': 128,
            'Ny': 128,
            'Nz': 1,
            'sets': 10,
            'farfield': True,
            'MAXIT': 6000,
            'TOL': 1e-8,
            'lambda_0': 0.5,
            'lambda_max': 0.50,
            'lambda_switch': 30,
            'data_ball': .999826e-30,
            'diagnostic': True,
            'iterate_monitor_name': 'FeasibilityIterateMonitor',
            'rotate': False,
            'verbose': 1,
            'graphics': 1,
            'anim': False,
            'debug': True
        }
        return defaultParams

    def __init__(self, **kwargs):
        super(Molecule3D, self).__init__(**kwargs)

        # do here any data member initialization

        # the following data members are set by loadData(), in addition to those specified in parent classes
        self.fmask = None  # holes in the data
        self.support = None  # support
        self.sparsity_support = None

    def setupProxOperators(self):
        super(Molecule3D, self).setupProxOperators()

        self.propagator = 'PropagatorFFTn'
        self.inverse_propagator = 'InvPropagatorFFTn'

    def loadData(self):
        """
        Load data and set in the correct format for reconstruction
        """
        # TODO: copy from data processor
        raise NotImplementedError

    def createRandomGuess(self):
        """
        Taking the measured data, add a random phase and calculate the resulting iterate guess
        """
        ph_init = 2 * np.pi * np.random.random_sample(self.data.shape)
        self.u0 = self.data * np.exp(1j * ph_init)
        self.u0 = np.fft.fftn(self.u0)

    def show(self):
        """
        Create result plots for the reconstruction
        Plot handles are stored in self.output['plots']
        """
        super(Molecule3D, self).show()
        # TODO: check whether u1 and u2 are the correct things
        phys_constraint_plot = XYZStackViewer(self.output['u1'],
                                              name='Physical constraint satisfied (%s)' % self.constraint)
        fourier_constraint_plot = XYZStackViewer(self.output['u2'], name='Fourier constraint satisfied')
        fourier_space_plot = XYZStackViewer(shifted_fft(self.output['u1']),
                                            name='Fourier space under physical constraint (%s)' % self.constraint)
        self.output['plots'].append(phys_constraint_plot)
        self.output['plots'].append(fourier_constraint_plot)
        self.output['plots'].append(fourier_space_plot)



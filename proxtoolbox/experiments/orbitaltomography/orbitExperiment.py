from proxtoolbox.experiments.experiment import Experiment
from matplotlib.pyplot import subplots, show
from datetime import datetime
import os
from glob import glob
from proxtoolbox.utils.h5 import write_dict_to_h5
import numpy as np


class OrbitalTomographyExperiment(Experiment):
    @staticmethod
    def getDefaultParameters():
        raise NotImplementedError("Static method getDefaultParameters() must \
                                  be implemented in any concrete class derived from \
                                  Experiment class")

    def __init__(self,
                 **kwargs):
        """
        Basic orbital tomography experiment class, to be extended with further functionality in a child class.
        """
        # call parent's __init__ method
        super(OrbitalTomographyExperiment, self).__init__(**kwargs)
        # do further initialization here

        self.input_ignore_attrs = ['input_ignore_attrs', 'output_ignore_keys',
                                   'proxOperators',
                                   'output', 'data_zeros',
                                   'figure_width', 'figure_height', 'figure_dpi']
        #                           ['dynamic_graphics', 'data', 'data_sq', 'rt_data',  'u0',
        #                            'truth', 'data_zeros', 'support',
        #                            'output', 'matlab_output', 'json_output', 'save_output_tf', 'pickle_output',
        #                            'h5_output', 'savedir', 'save_name']
        self.output_ignore_keys = ['u1', 'u2', 'plots']

    def loadData(self):
        """
        Load or generate the dataset that will be used for
        this experiment. Create the initial iterate.
        """
        raise NotImplementedError("This is just an abstract interface")

    def reshapeData(self, Nx, Ny, Nz):
        """
        Remnant of experiment class, called after loadData during initialization
        """
        pass

    def setupProxOperators(self):
        """
        Determine the prox operators to be used based on the given constraint.
        This method is called during the initialization process.

        sets the parameters:
        - self.proxOperators
        - self.propagator and self.inverse_propagator
        """
        # Set self.proxOperators based on known operators from base Experiment
        super(OrbitalTomographyExperiment, self).setupProxOperators()
        # Search for sparsity-based proxoperators
        if self.constraint == 'sparse real':
            self.proxOperators.append('P_Sparsity_real')
        elif self.constraint == 'sparse complex':
            self.proxOperators.append('P_Sparsity')
        elif self.constraint in ['symmetric sparse real', 'sparse symmetric real']:
            self.proxOperators.append('P_Sparsity_Symmetric_real')
        elif self.constraint in ['symmetric sparse complex', 'symmetric sparse complex']:
            self.proxOperators.append('P_Sparsity_Symmetric')

        # Modulus proxoperator (normally the second operator)
        if self.experiment_name == '3D ARPES':
            self.proxOperators.append('P_M_masked')
            self.propagator = 'PropagatorFFTn'
            self.inverse_propagator = 'InvPropagatorFFTn'
        elif self.experiment_name == 'noisy 2D ARPES':
            self.proxOperators.append('Approx_P_M')
            self.propagator = 'PropagatorFFT2'
            self.inverse_propagator = 'InvPropagatorFFT2'
        else:
            # self.proxOperators.append('Approx_Pphase_FreFra_Poisson')  # Old implementation
            self.proxOperators.append('P_M')
            self.propagator = 'PropagatorFFT2'
            self.inverse_propagator = 'InvPropagatorFFT2'

        self.nProx = len(self.proxOperators)

    def show(self):
        """
        Basic plotting routine, plots the results of the algorithm. Can be extended with further plots in child classes
        References to the plots can be stored in self.output['plots']
        :return:
        """
        self.output['plots'] = []
        convergence_plots, ax = subplots(1, 2, figsize=(7, 3))
        ax[0].semilogy(self.output['stats']['change'])
        ax[0].set_title('Change')
        ax[0].set_xlabel('iteration')
        ax[0].set_ylabel('$||x^{2k+2}-x^{2k}||$')
        if 'gaps' in self.output['stats']:
            gaps = self.output['stats']['gaps']
            ax[1].semilogy(gaps)
            ax[1].set_xlabel('iteration')
            ax[1].set_title('Gap')
            ax[1].set_ylabel('$||x^{2k+1}-x^{2k}||$')
        convergence_plots.tight_layout()
        show()
        self.output['plots'].append(convergence_plots)

    @property
    def input_params(self):
        """Return a dictionary describing the input parameters.
        Not included are subclasses and multi-dimensional lists."""
        vars_dict = vars(self)
        output = vars_dict.copy()
        for var in vars_dict:
            obj = vars_dict[var]
            # Exclude some variables:
            if var in self.input_ignore_attrs:
                output.pop(var)
                continue

            # Only include variables that are not an instance of a class or large numpy arrays
            if hasattr(obj, '__dict__') is True:
                output.pop(var)
                continue
            if (isinstance(obj, (list, np.ndarray)) and len(obj) > 0 and
                    isinstance(obj[0], (list, np.ndarray)) and len(obj[0]) > 1):
                output.pop(var)
                continue
        return output

    @property
    def output_for_saving(self):
        """ Create a dictionary of the output which can be saved. """
        output = {}
        for key in self.output:
            if key in self.output_ignore_keys:
                pass
            elif key == 'u_monitor':
                # replace list with a dict
                length = len(self.output[key])
                output[key] = {('%d' % i): self.output[key][i] for i in range(length)}
            else:
                output[key] = self.output[key]
        return output

    def saveOutput(self, savedir='', name=''):
        """
        Save all input and output of the reconstruction
        Path is given by savedir + date(YYYY_MM_DD) + name + "/". This directory is created if not previously existing

        :param savedir: main directory path
        :param name: sub-directory name
        """
        # Create directory (if needed); does not raise an exception if the directory already exists
        date_time = datetime.now()  # current date and time
        date_str = date_time.strftime("%Y_%m_%d")
        if savedir == '':
            savedir = os.curdir
        if name != '':  # connect date and name using underscore
            date_str += '_'
        directory = savedir + date_str + name
        os.makedirs(directory, exist_ok=True)

        file_id = 1
        while len(glob(directory + "\\%04d*" % file_id)) > 0:
            file_id += 1
        filename_root = ("\\%04d" % file_id) + "_" + self.experiment_name + '_' + self.algorithm_name

        # prepare a special version of the output before saving it
        output = self.output_for_saving
        # Also for the input
        exp_input = self.input_params

        # Save HDF5 output
        full_output = {'input': exp_input, 'output': output}
        h5_file_path = directory + filename_root + '.hdf5'
        write_dict_to_h5(h5_file_path, full_output)

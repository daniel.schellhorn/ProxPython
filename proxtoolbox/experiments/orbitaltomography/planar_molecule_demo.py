import sys, os
# set proxpython path
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../../..")

from proxtoolbox.experiments.orbitaltomography import planar_molecule as pm

if __name__ == "__main__":  # prevent execution when generating documentation
    data_file = '..\\..\\..\\InputData\\OrbitalTomog\\coronene_homo1.tif'
    exp_pm = pm.PlanarMolecule(data_filename=data_file,
                            experiment='noisy 2D ARPES',  # 'noisy 2D ARPES' OR '2D ARPES'
                            constraint='sparse real',
                            use_sparsity_with_support=True,
                            sparsity_parameter=40,
                            TOL=1e-10,
                            lambda_0=0.85, lambda_max=0.5, lambda_switch=30,
                            rnd_seed=None)  # Disable fixed pseudo-random number generator
    # exp_pm.plotInputData()
    exp_pm.run()
    exp_pm.show()


from proxtoolbox.experiments.phase.phaseExperiment import PhaseExperiment
from proxtoolbox import proxoperators
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils import fft, ifft
from proxtoolbox.utils.loadMatFile import loadMatFile
from proxtoolbox.utils.accessors import accessors
import proxtoolbox.utils as utils
import numpy as np
from numpy import sqrt, conj, tile, mean, exp, angle, trace, reshape, matmul
from numpy.random import randn, random_sample
from numpy.linalg import norm
from numpy.fft import fft2, ifft2
import time


class CDP_Experiment(PhaseExperiment):
    """
    CDP experiment class
    """

    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'CDP',
            'object': 'complex',
            'constraint': 'hybrid',
            'Nx': 1,
            'Ny': 128,
            'Nz': 1,
            'sets': 10,
            'product_space_dimension': 10,
            'farfield': True,
            'MAXIT': 10000,
            'TOL': 1e-10,
            'lambda_0': 0.55,
            'lambda_max': 0.55,
            'lambda_switch': 20,
            'data_ball': 1e-30,
            'diagnostic': True,
            'iterate_monitor_name': 'FeasibilityIterateMonitor',
            'verbose': 0,
            'graphics': 1,
            'anim': False,
            'debug':True
        }
        return defaultParams

    def __init__(self, 
                 warmup_iter = 0,
                 **kwargs):
        """
        """
        # call parent's __init__ method
        super(CDP_Experiment, self).__init__(**kwargs)

        # do here any data member initialization
        self.warmup_iter = warmup_iter

        # the following data members are set by loadData()
        self.masks = None
        self.indicator_ampl = None
        self.abs_illumination = None
        self.illumination = None
        self.support_idx = None


    def loadData(self):
        """
        Load CDP dataset. Create the initial iterate.
        """
        # Implementation of the Wirtinger Flow (WF) algorithm presented in the paper 
        # "Phase Retrieval via Wirtinger Flow: Theory and algorithms"
        # by E. J. Candes, X. Li, and M. Soltanolkotabi
        # integrated into the ProxToolbox by 
        # Russell Luke, September 2016.
        
        # The input data are coded diffraction patterns about a random complex
        # valued image. 

        # for debug:
        # used predefined randomly generated data for the case where n1 = 128, n2 = 1
        n1 = self.Ny
        n2 = self.Nx
        debug = self.debug and n1 == 128 and (n2 == 1 or n2 == 256)   \
                and self.product_space_dimension == 10
        debug_image = None
        debug_masks = None
        debug_z0 = None
        
        # make image
        if debug:
            if n2 == 1:
                x_dict = loadMatFile('../InputData/Phase/CDP_1D_x.mat')
                debug_image = x_dict['x']
                masks_dict = loadMatFile('../InputData/Phase/CDP_1D_Masks.mat')
                debug_masks = masks_dict['Masks']
                z0_dict = loadMatFile('../InputData/Phase/CDP_1D_z0.mat')
                debug_z0 = z0_dict['z0']
            elif n2 == 256:
                x_dict = loadMatFile('../InputData/Phase/CDP_2D_x.mat')
                debug_image = x_dict['x']
                masks_dict = loadMatFile('../InputData/Phase/CDP_2D_Masks.mat')
                debug_masks = masks_dict['Masks']
                z0_dict = loadMatFile('../InputData/Phase/CDP_2D_z0.mat')
                debug_z0 = z0_dict['z0']
            x = debug_image
        else:
            x = randn(n1,n2) + 1j*randn(n1,n2)
        self.truth = x
        self.norm_truth = norm(x,'fro')
        self.truth_dim = x.shape
            
        ## Make masks and linear sampling operators
        L = self.product_space_dimension  # Number of masks 
        if debug:
            masks = debug_masks
        else:
            if n2 == 1:
                masks = np.random.choice(np.array([1j, -1j, 1, -1]),(n1,L))
            elif n1 == 1:
                masks = np.random.choice(np.array([1j, -1j, 1, -1]),(L,n2))
            else:
                masks = np.random.choice(np.array([1j, -1j, 1, -1]),(n1,n2,L))
        
            # Sample magnitudes and make masks 
            temp = random_sample(masks.shape) #works like rand but accepts tuple as argument
            masks = masks * ( (temp <= 0.2)*sqrt(3) + (temp > 0.2)/sqrt(2) )

        self.masks = conj(masks) # Saving the conjugate of the mask saves
                                 # on computing the conjugate every time the
                                 # mapping A (below) is applied.       
        self.masks_conj = masks  # what we call masks_conj are actually the
                                 # original masks. We  do this so as not to break
                                 # code that used masks. 
                                 # Would be nice to sort this out.
        # Make linear operators; A is forward map and At its scaled adjoint (At(Y)*numel(Y) is the adjoint)
        if n2 == 1:
            A = lambda I: fft(conj(masks) * tile(I,[1, L]))  # Input is n x 1 signal, output is n x L array
            At = lambda Y: mean(masks * ifft(Y), 1).reshape((n1,n2))  # Input is n x L array, output is n x 1 signal
        elif n1 == 1 :
            A = lambda I: fft(conj(masks) * tile(I,[L, 1]))  # Input is 1 x n signal, output is L x n array
            At = lambda Y: mean(masks * ifft(Y), 0).reshape((n1,n2))  # Input is L x n array, output is 1 x n signal
        else:
            A = lambda I: self.A(I)  # Input is n1 x n2 image, output is n1 x n2 x L array
            At = lambda Y: self.At(Y)  # Input is n1 x n2 X L array, output is n1 x n2 image
        
        # Support constraint: none
        self.indicator_ampl = 1
        self.abs_illumination = 1
        self.illumination = 1
        if n1 == 1 or n2 == 1:
            self.support_idx = np.nonzero(x.flatten(order = 'F')) # column-major order'
        else:
            self.support_idx = np.nonzero(x)
        
        # Data
        Y = abs(A(x))
        self.rt_data = Y
        Y = Y**2
        self.data = Y
        self.norm_data = sum(Y.flatten())/Y.size        
        normest = sqrt(self.norm_data) # Estimate norm to scale eigenvector 
        self.norm_rt_data = normest

        # Initialization     
        npower_iter = self.warmup_iter  # Number of power iterations 
        if debug: 
            z0 = debug_z0
        else:
            z0 = randn(n1,n2)
        z0 = z0/norm(z0,'fro') # Initial guess 
        
        _tic = time.time()             
        
        # Power iterations 
        for _tt in range(npower_iter): 
            z0 = At(Y*A(z0))
            z0 = z0/norm(z0)
        
        _toc  = time.time()

        z = normest * z0  # Apply scaling               
        if n2 == 1:
            _Relerrs = norm(x - exp(-1j*angle(trace(matmul(x.T.conj(), z)))) * z, 'fro')/norm(x,'fro')
            self.u0 = tile(z,[1,L])
        elif n1 == 1:
            _Relerrs = norm(x - exp(-1j*angle(trace(matmul(z.T.conj(), x)))) * z, 'fro')/norm(x,'fro')
            self.u0 = tile(z,[L,1])
        else:
            _Relerrs = norm(x - exp(-1j*angle(trace(matmul(x.T.conj(), z)))) * z, 'fro')/norm(x,'fro')
            self.u0 = reshape(tile(z,[1, L]), (z.shape[0], z.shape[1], L), order='F') # order='F': to match Matlab
        
        if self.formulation == 'cyclic':
            if self.Nx == 1:
                self.u0 = self.u0[:,0]
            elif self.Ny == 1:
                self.u0 = self.u0[0,:]
            else:
                self.u0 = self.u0[:,:,0]
        else:
            # TODO need to check that the following code
            # converted from Matlab makes sense
            self.product_space_dimension = self.sets
            self.sets = 2

            # Originally the data layout is not using cells.
            # Turns out that the code is about twice as fast 
            # when using cells.
            # It will be best to use cells right from the beginning,
            # but for now we allow to switch back and forth between
            # the two data layouts
            useCells = True
            if useCells:
                L = self.product_space_dimension
                get, _set, _data_shape = accessors(self.u0, self.Nx, self.Ny, L)
                u0 = self.u0
                self.u0 = Cell(L)
                data = self.data
                self.data = Cell(L)
                rt_data = self.rt_data
                self.rt_data = Cell(L)
                masks = self.masks
                self.masks = Cell(L)
                masks_conj = self.masks_conj
                self.masks_conj = Cell(L)
                for j in range(L):
                    self.u0[j] = get(u0, j)
                    self.data[j] = get(data, j)
                    self.rt_data[j] = get(rt_data, j)
                    self.masks[j] = get(masks, j)
                    self.masks_conj[j] = get(masks_conj, j)

        
        #print('Run time of initialization:', toc-tic, 's')
        #print('Relative error after initialization:', Relerrs)
        #print('\n')

    def A(self, x):
        '''
        Forward map (linear operator)
        Works on two-dimensional data and produces 
        correct behavior (same as Matlab)
        ''' 
        L = self.product_space_dimension
        tmp_x = reshape(tile(x,[1, L]), (x.shape[0],x.shape[1], L), order='F')
        masks_x = self.masks*tmp_x
        fft2_masks_x = np.empty(self.masks.shape, dtype=self.masks.dtype)
        for j in range(L):
            fft2_masks_x[:,:,j] = fft2(masks_x[:,:,j])
        return fft2_masks_x

    def At(self, Y):
        '''
        Scaled adjoint of A (linear operator)
        Works on two-dimensional data and produces 
        correct behavior (same as Matlab)
        ''' 
        L = self.product_space_dimension
        ifft2_Y = np.empty(Y.shape, dtype=Y.dtype)
        for j in range(L):
            ifft2_Y[:,:,j] = ifft2(Y[:,:,j])
        return mean(self.masks_conj*ifft2_Y, 2)


    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(CDP_Experiment, self).setupProxOperators() # call parent's method
        
        if self.formulation == 'cyclic':
            self.proxOperators = [] 
            self.nProx = self.sets
            for _j in range(self.nProx):
                self.proxOperators.append('P_CDP_cyclic')
        else:
            #input.Prox0 = input.Prox{1}; # used in ADMM
            self.n_product_Prox = self.product_space_dimension
            self.proxOperators = []
            self.proxOperators.append('P_diag')
            self.proxOperators.append('P_CDP')
        
        self.propagator = 'Propagator_FreFra'
        self.inverse_propagator = 'InvPropagator_FreFra'

         # Now we adjust data structures and prox operators according to the algorithm
        if self.algorithm_name == 'ADMM':
            # ADMM has three blocks of variables, primal, auxilliary (primal
            # variables in the image space of some linear mapping) and dual. 
            # we sort these into a 3D cell.
            u0 = self.u0
            self.u0 = Cell(3)
            self.proxOperators = []
            self.productProxOperators = []
            L = self.product_space_dimension
            get, _set, _data_shape = accessors(u0, self.Nx, self.Ny, L)
            self.u0[0] = get(u0, 0)
            if isCell(u0):
                u0_1 = Cell(L)
                u0_2 = Cell(L)
                for j in range(L):
                    u0_1[j] = u0[j].copy()
                    u0_2[j] = u0[j]/L
                self.u0[1] = u0_1
                self.u0[2] = u0_2
            else:
                self.u0[1] = u0.copy()
                self.u0[2] = u0/L

            self.proxOperators.append('Prox_primal_ADMM_indicator')
            self.proxOperators.append('P_CDP_ADMM') 
        elif self.algorithm_name == 'AvP2':
            # AvP2 has three blocks of variables, primal, auxilliary (primal
            # variables in the image space of some linear mapping) and dual. 
            # we sort these into a 3D cell.
            u0 = self.u0
            self.u0 = Cell(3)
            #self.proxOperators = []
            self.productProxOperators = []
            L = self.product_space_dimension
            get, _set, _data_shape = accessors(u0, self.Nx, self.Ny, L)

            self.u0[0] = get(u0, 0)

            prox2_class = getattr(proxoperators, self.proxOperators[1])
            prox2 = prox2_class(self)
            self.u0[2] = prox2.eval(u0)

            P_Diag_class = getattr(proxoperators, 'P_diag')
            P_Diag_prox = P_Diag_class(self)
            tmp_u = P_Diag_prox.eval(self.u0[2])
            self.u0[1] = get(tmp_u, 0)

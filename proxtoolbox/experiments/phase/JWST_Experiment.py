
from proxtoolbox.experiments.phase.phaseExperiment import PhaseExperiment
from proxtoolbox import proxoperators
import numpy as np
from numpy import fromfile, exp, nonzero, zeros, pi, resize, real, angle
from numpy.random import rand
from numpy.linalg import norm
from numpy.fft import fftshift, fft2

import proxtoolbox.utils as utils
from proxtoolbox.utils.cell import Cell, isCell

#for downloading data
import proxtoolbox.utils.GetData as GetData

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure


class JWST_Experiment(PhaseExperiment):
    '''
    JWST experiment class
    '''

    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name' : 'JWST',
            'object': 'complex',
            'constraint': 'amplitude only',
            'distance': 'far field',
            'Nx': 128,
            'Ny': 128,
            'Nz': 1,
            'product_space_dimension': 4,
            'MAXIT': 6000,
            'TOL': 5e-5,
            'lambda_0': 0.85,
            'lambda_max': 0.85,
            'lambda_switch': 20,
            'data_ball': 1e-30,
            'diagnostic': True,
            'iterate_monitor_name': 'FeasibilityIterateMonitor',
            'verbose': 0,
            'graphics': 1
        }
        return defaultParams
    

    def __init__(self, **kwargs):
        super(JWST_Experiment, self).__init__(**kwargs)
        self.fresnel_nr = None
        self.use_farfield_formula = None
        self.data_zeros = None
        self.supp_ampl = None
        self.indicator_ampl = None
        self.abs_illumination = None
        self.illumination_phase = None
        self.FT_conv_kernel = Cell(3)

    def loadData(self):
        """
        Load JWST dataset. Create the initial iterate.
        """
        #make sure input data can be found, otherwise download it
        GetData.getData('Phase')

        # following code is not used anymore in Cone_and_Sphere
        """
        if experiment.distance is not None:
            if experiment.distance == 'near field':
                self.fresnel_nr = 1*2*np.pi*experiment.Nx
                self.use_farfield_formula = 0
            else:
                self.fresnel_nr = 0
                self.use_farfield_formula = 1   
        """

        # So far Matlab code used only Nx to set the desired
        # resolution
        newres = self.Nx
        
        # The following value for snr does not work well in Python
        # as this creates numerical issues with Poisson noise 
        # (data_ball is far too small)
        # snr = self.data_ball
        # Instead, we choose the following bigger value which  
        # works well.
        snr = 1e-8 

        print('Loading data.')
        with open('../InputData/Phase/pupil.pmod','r') as fid:
        # read lower endian float <f
            Xi_A = np.fromfile(fid, dtype='<f')
            Xi_A = Xi_A.astype(np.float64)
            Xi_A = Xi_A.reshape((512,512)).T

        diversity = 3

        with open('../InputData/Phase/phase_p37.pmod','r') as fid:
        # read lower endian float <f
            temp1 = np.fromfile(fid, dtype='<f')
            temp1 = temp1.astype(np.float64)
            temp1 = temp1.reshape(512,512).T

        with open('../InputData/Phase/phase_m37.pmod','r') as fid:
        # read lower endian float <f
            temp2 = np.fromfile(fid, dtype='<f')
            temp2 = temp2.astype(np.float64)
            temp2 = temp2.reshape(512,512).T

        defocus = (temp1-temp2)/2
        theta = (temp1+temp2)/2

        if newres != 512:
            defocus = utils.Resize(defocus, newres, newres)
            theta = utils.Resize(theta, newres, newres)
            Xi_A = utils.Resize(Xi_A, newres, newres)

        self.supp_ampl = nonzero(Xi_A)
        self.indicator_ampl = np.zeros(Xi_A.shape)
        self.indicator_ampl[self.supp_ampl] = 1
        self.product_space_dimension = diversity + 1
        self.sets = diversity + 1

        #aberration = np.array( [np.zeros((newres,newres)), defocus, -defocus])
        aberration = Cell(3)
        aberration[0] = np.zeros((newres,newres))
        aberration[1] = defocus
        aberration[2] = -defocus
        #aberration = [np.zeros((newres,newres)), defocus, -defocus]

        true_object = Xi_A*exp(1j*(theta))
        k = Cell(3)
        rt_k = Cell(3)
        #k = []
        #rt_k = []
        norm_rt_data = np.sqrt(sum(sum(Xi_A)))
        data_zeros = []
        data_zeros = Cell(3)
        for j in range(len(aberration)):
            aberration_j = aberration[j]
            Pj = Xi_A*exp(1j*(aberration_j+theta))
            k_j = np.square(abs(utils.FFT(Pj)))
            rt_k_j = np.sqrt(k_j)
            if self.noise:
                # Add Poisson noise according to Ning Lei:
                # f = alpha*4*4*pi/q/q*abs(cos(qx(iX))*sin(qy(iY))*(sin(q)/q-cos(q)));
                #        f2 = PoissonRan(f*f*2)/alpha/alpha/2;   # 2 is for I(-q)=I(q)
                #	sigma(iX, iY, iZ) = f/np.sqrt(2)/alpha/alpha/abs(ff)/abs(ff);
                # July 15, 2010:  Since this is meant to model photon counts (which
                # are integers) we add noise and then round down
                k_j = k_j/snr
                for ii in range(newres):
                    for jj in range(newres):
                        k_j[ii,jj]= utils.PoissonRan(k_j[ii,jj])*snr # this matches what is currently done in Matlab
                        #k_j[ii,jj]= np.random.poisson(k_j[ii,jj])*snr # does not work: throw an exception if snr is too small 
                k_j = np.round(k_j)
                rt_k_j = np.sqrt(k_j)

            # fftshift and scale the data for use with fft:
            rt_k_j = fftshift(rt_k_j)/newres

            k[j] = k_j
            rt_k[j] = rt_k_j
            zeros = np.where(rt_k_j == 0)
            data_zeros[j] = zeros
            self.FT_conv_kernel[j] = self.indicator_ampl*exp(1j*aberration_j)


        self.rt_data = rt_k
        self.data = k
        self.norm_rt_data = norm_rt_data # this is correct since
                                         # is is calculated in the 
                                         # object domain
        self.data_zeros = data_zeros
        self.abs_illumination = Xi_A
        # Normalize the illumination so that it has the 
        # same energy as the data.
        self.abs_illumination = self.abs_illumination/norm(self.abs_illumination)*self.norm_rt_data
        # self.illumination_phase = aberration -> now input.FT_conv_kernel

        # initial guess
        perturb = (np.random.rand(newres, newres)).T # to match Matlab 
        if self.formulation == "cyclic":
            self.u0 = self.abs_illumination*exp(1j*2*pi*perturb)
        else: # product space
            self.u0 = Cell(self.sets)
            for j in range(self.sets):
                u0_j = self.abs_illumination*exp(1j*2*pi*perturb) 
                self.u0[j] = u0_j

        self.truth = true_object
        self.truth_dim = true_object.shape
        self.norm_truth = norm(true_object)

    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(JWST_Experiment, self).setupProxOperators()  # call parent's method

        # add propagator and inverse propagator
        # used by Approx_Pphase_FreFra_Poisson prox operator
        # we do this now as those are needed when instantiating 
        # Approx_Pphase_FreFra_Poisson below
        self.propagator = 'Propagator_FreFra'
        self.inverse_propagator = 'InvPropagator_FreFra'

        self.proxOperators = []
        self.productProxOperators = []
        if self.formulation == 'cyclic':
            # there are as many prox operators as there are sets
            self.nProx = self.sets
            self.product_space_dimension = 1
            for _j in range(self.nProx-1):
                self.proxOperators.append('Approx_Pphase_FreFra_Poisson')
            self.proxOperators.append('P_amp_support')
        else: # product space formulation
            # add prox operators           
            self.nProx = 2
            self.proxOperators.append('P_diag')
            self.proxOperators.append('Prox_product_space')
            # add product prox operators
            self.n_product_Prox = self.product_space_dimension
            for _j in range(self.n_product_Prox-1):
                self.productProxOperators.append('Approx_Pphase_FreFra_Poisson')
            self.productProxOperators.append('P_amp_support')

        # Now we adjust data structures and prox operators according to the algorithm
        if self.algorithm_name == 'ADMM':
            # set-up variables in cells for block-wise algorithms
            # ADMM has three blocks of variables, primal, auxilliary (primal
            # variables in the image space of some linear mapping) and dual. 
            # we sort these into a 3D cell.
            u0 = self.u0
            self.u0 = Cell(3)
            self.proxOperators = []
            self.productProxOperators = []
            K = self.product_space_dimension - 1
            self.product_space_dimension = K
            self.u0[0] = u0[K]
            prox_FreFra_Poisson_class = getattr(proxoperators, 'Approx_Pphase_FreFra_Poisson')
            prox_FreFra_Poisson = prox_FreFra_Poisson_class(self)
            self.u0[1] = Cell(K)
            self.u0[2] = Cell(K)
            for j in range(K):
                tmp_u0 = prox_FreFra_Poisson.eval(u0[j], j)
                self.u0[1][j] = fft2(self.FT_conv_kernel[j]*tmp_u0) / (self.Nx*self.Ny)
                self.u0[2][j] = self.u0[1][j] / self.lambda_0
            self.proxOperators.append('Prox_primal_ADMM_indicator')
            self.proxOperators.append('Approx_Pphase_FreFra_ADMM_Poisson') 
        elif self.algorithm_name == 'AvP2':
            # u_0 should be a cell...we change it into a cell of cells
            u0 = self.u0
            self.u0 = Cell(3)
            prox2_class = getattr(proxoperators, self.proxOperators[1])
            prox2 = prox2_class(self)
            self.u0[2] = prox2.eval(u0)
            P_Diag_class = getattr(proxoperators, 'P_diag')
            P_Diag_prox = P_Diag_class(self)
            tmp_u = P_Diag_prox.eval(self.u0[2])
            self.u0[1] = tmp_u[0]
            self.u0[0] = u0[self.product_space_dimension-1]      
        elif self.algorithm_name == 'PHeBIE':
            # set up variables in cells for block-wise, implicit-explicit algorithms
            u0 = self.u0
            self.u0 = Cell(2)
            self.product_space_dimension = self.product_space_dimension - 1
            self.u0[0] = u0[self.product_space_dimension].copy()  # copy last element of prev u0 into first slot
            self.u0[1] = Cell(self.product_space_dimension)
            for j in range(self.product_space_dimension):
                self.u0[1][j] = u0[j]
            self.proxOperators = []
            self.nProx = 2
            self.proxOperators.append('P_amp_support')  # project onto the support/amplitude constraints
            self.proxOperators.append('Prox_product_space')
            self.productProxOperators = []
            self.n_product_Prox = self.product_space_dimension
            for _j in range(self.n_product_Prox):
                self.productProxOperators.append('Approx_Pphase_FreFra_Poisson')
        elif self.algorithm_name == 'Wirtinger':
            # Contrary to the Matlab code, we use cells instead of 3D arrays
            L = len(self.FT_conv_kernel)
            self.product_space_dimension = L
            self.masks = Cell(L)
            for j in range(L):
                self.masks[j] = self.indicator_ampl * self.FT_conv_kernel[j]
            self.data_zeros = None 
            self.FT_conv_kernel = self.masks
            self.proxOperators[1] = 'Approx_Pphase_JWST_Wirt'
            self.use_farfield_formula = True
            self.fresnel_nr = 0

    def show(self):
        """
        Generate graphical output from the solution
        """
        u_m = self.output['u_monitor']
        if isCell(u_m):
            u = u_m[0]
            if isCell(u):
                u = u[0]
            u2 = u_m[len(u_m)-1]
            if isCell(u2):
                u2 = u2[len(u2)-1]
        else:
            u2 = u_m
            if u2.ndim > 2:
                u2 = u2[:,:,0]
            u = self.output['u']
            if isCell(u):
                u = u[0]
            elif u.ndim > 2:
                u = u[:,:,0]

        algo_desc = self.algorithm.getDescription()
        title = "Algorithm " + algo_desc

        # figure 904
        titles = ["Best approximation amplitude - physical constraint satisfied",
                  "Best approximation phase - physical constraint satisfied",
                  "Best approximation amplitude - Fourier constraint satisfied",
                  "Best approximation phase - Fourier constraint satisfied"]
        f = self.createFourImageFigure(u, u2, titles)
        f.suptitle(title)
        plt.subplots_adjust(hspace = 0.3) # adjust vertical space (height) between subplots (default = 0.2)

        # figure 900
        changes = self.output['stats']['changes']
        time = self.output['stats']['time']
        time_str = "{:.{}f}".format(time, 5) # 5 is precision
        label = "Iterations (time = " + time_str + " s)"

        f, ((ax1, ax2), (ax3, ax4)) = subplots(2, 2, \
                    figsize = (self.figure_width, self.figure_height),
                    dpi = self.figure_dpi)
        self.createImageSubFigure(f, ax1, abs(u), titles[0])
        self.createImageSubFigure(f, ax2, real(u), titles[1])
        
        ax3.semilogy(changes)
        ax3.set_xlabel(label)
        ax3.set_ylabel('Log of change in iterates')

        if 'gaps' in self.output['stats']:
            gaps = self.output['stats']['gaps']
            ax4.semilogy(gaps)
            ax4.set_xlabel(label)
            ax4.set_ylabel('Log of the gap distance')

        f.suptitle(title)
        plt.subplots_adjust(hspace = 0.3) # adjust vertical space (height) between subplots (default = 0.2)
        plt.subplots_adjust(wspace = 0.3) # adjust horizontal space (width) between subplots (default = 0.2)
        
        show()

  


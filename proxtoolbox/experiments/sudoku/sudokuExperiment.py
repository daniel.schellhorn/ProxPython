
# pylint: disable=no-member # for dynamically created variables
# pylint: disable=access-member-before-definition # for dynamically created variables
from proxtoolbox.experiments.experiment import Experiment
from proxtoolbox import proxoperators

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure


class SudokuExperiment(Experiment):
    '''
    Sudoku experiment class

    The goal of a standard Sudoku puzzle is to fill a 9x9 array
    with the numbers from 1 to 9. Every row, every column and each
    of the 9 3x3 subblocks should contain each number only once.
    
    Starting point is a grid that already contains several numbers.
    Usually, there exists a unique solution.
     
    '''
    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name' : 'Sudoku',
            'Nx':9,
            'Ny':9,
            'Nz':9,
            'product_space_dimension':4,
            'algorithm': 'DRl',
            'MAXIT': 2000,
            'TOL': 1e-9,
            'TOL2': 1e-4,
            'lambda_0': 1,
            'lambda_max': 1,
            'lambda_switch': 1,
            'norm_data':81,
            'sudoku':((2,0,0,0,0,1,0,3,0),
                    (4,0,0,0,8,6,1,0,0),
                    (0,0,0,0,0,0,0,0,0),
                    (0,0,0,0,1,0,0,0,0),
                    (0,0,0,0,0,0,9,0,0),
                    (0,0,5,0,0,3,0,0,7),
                    (0,0,0,0,0,0,0,0,0),
                    (1,0,0,0,0,7,4,9,0),
                    (0,2,4,1,0,0,0,0,0)),
            'diagnostic': True
        }
        return defaultParams


    def __init__(self, sudoku = None, norm_data=81, **kwargs):
        """
        """
        # call parent's __init__ method
        super(SudokuExperiment, self).__init__(**kwargs)
        
        # do here any data member initialization
        self.norm_data = norm_data
        if sudoku is None:
            defaultParams = SudokuExperiment.getDefaultParameters()
            self.sudoku = defaultParams['sudoku']        
        self.sudoku = np.array(sudoku, dtype=np.float32)
 

    def loadData(self):
        """
        Create the initial iterate based on the given Sudoku puzzle.
        """
        u0 = np.zeros((self.Nx, self.Ny, self.Nz, self.product_space_dimension),
                      dtype=self.sudoku.dtype)
        for x in range(self.Nx):
            for y in range(self.Ny):
                z = int(self.sudoku[x][y]-1)
                if z >= 0:
                    u0[x,y,z,:] = 1
        
        self.u0 = u0


    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(SudokuExperiment, self).setupProxOperators() # call parent's method
        
        self.nProx = 2
        self.proxOperators = ['Prox_product_space', 'P_diag']
        self.n_product_Prox = self.product_space_dimension
        self.productProxOperators = ['ProjRow','ProjColumn','ProjSquare','ProjGiven']
  
  
    def postprocess(self):
        """
        Process the data returned by the algorithm
        """
        solution = np.zeros_like(self.sudoku)
        u2 = self.algorithm.prox2.eval(self.output['u'])
        u1 = self.algorithm.prox1.eval(u2)

        A = u1[:,:,:,0]
        
        for x in range(self.Nx):
            for y in range(self.Ny):
                for z in range(self.Nz):
                    if A[x,y,z] > 0:
                        solution[x,y] = z+1
                        break 

        self.solution = solution

    def show(self):

        """
        Generates graphical output from the solution
        """
        fig = plt.figure('Sudoku', figsize = (self.figure_width, self.figure_height),
                         dpi = self.figure_dpi)
        
        # plot plain Sudoku puzzle
        ax = plt.subplot(2,2,1)
        ax.title.set_text('Given Sudoku')
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
        table = ax.table(cellText=self.sudoku.astype(np.int32),
                         loc='center', cellLoc='center')
        fontSize = 15
        table.set_fontsize(fontSize)
        for cell in table.properties()['children']:
            cell.set_height(0.1)
            cell.set_width(0.1)
            txt = cell.get_text()
            if txt.get_text() == '0':
                txt.set_text('')
                
        # plot solution
        ax = plt.subplot(2,2,2)
        ax.title.set_text('Solution')
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
        table = ax.table(cellText=self.solution.astype(np.int32),
                         loc='center', cellLoc='center')
        table.set_fontsize(fontSize)
        for cell in table.properties()['children']:
            cell.set_height(0.1)
            cell.set_width(0.1)
        
        # plot the change from one iterate to the next
        stats = self.output['stats']
        changes = stats['changes']
        ax = plt.subplot(2,2,3)
        ax.xaxis.label.set_text('Iterations')
        ax.yaxis.label.set_text('Change')
        plt.semilogy(changes)
        
        # plot the gap
        if 'gaps' in stats:
            gaps = stats['gaps']
            ax = plt.subplot(2,2,4)
            ax.xaxis.label.set_text('Iterations')
            ax.yaxis.label.set_text('Gap')
            plt.semilogy(gaps)
        
        fig.tight_layout()
        plt.show()

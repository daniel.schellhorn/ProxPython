from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj
from numpy import where, log, exp, sum

__all__ = ['P_M', 'P_M_masked', "Approx_P_M", 'Approx_P_M_masked']


class P_M(ProxOperator):
    """
    Projection onto Fourier magnitude constraints
    """

    def __init__(self, experiment):
        """
        Initialization
        
        Parameters
        ----------
        config : dict - Dictionary containing the problem configuration. It must contain the following mapping:
        'M' : array_like - non-negative real FOURIER DOMAIN CONSTRAINT
        """
        self.data = experiment.data
        self.prop = experiment.propagator(experiment)
        self.invprop = experiment.inverse_propagator(experiment)

    def eval(self, u, prox_idx=None):
        """
        Applies the proxoperator P_M
        
        Parameters
        ----------
        u : array_like - function in the physical domain to be projected
        
        Returns
        -------
        array_like - [p_M,phat_M] ,where p_M = the projection IN THE PHYSICAL (time) DOMAIN
            and phat_M = projection IN THE FOURIER DOMAIN
        """
        m = self.data
        a = self.prop.eval(u)
        b = magproj(m, a)
        return self.invprop.eval(b)


class P_M_masked(P_M):
    """
    Projection onto Fourier magnitude constraints, leaving alone points masked by 'fmask'
    """

    def __init__(self, experiment):
        """
        Initialization

        Parameters
        ----------
        experiment: experiment class
        """
        super(P_M_masked, self).__init__(experiment)
        self.mask = experiment.fmask == 0

    def eval(self, u, prox_idx=None):
        """
        Applies the proxoperator P_M_masked

        Parameters
        ----------
        u : array_like - function in the physical domain to be projected

        Returns
        -------
        array_like - p_M: the projection IN THE PHYSICAL (time) DOMAIN
        """
        fourier_space_iterate = self.prop.eval(u)
        constrained = magproj(self.data, fourier_space_iterate.copy())
        update = where(self.mask, fourier_space_iterate, constrained)
        return self.invprop(update)


class Approx_P_M(P_M):
    def __init__(self, experiment):
        super(Approx_P_M, self).__init__(experiment)
        self.data_sq = self.data ** 2
        self.data_zeros = where(self.data == 0)
        self.data_ball = experiment.data_ball
        self.TOL2 = experiment.TOL2

    def eval(self, u, prox_idx=None):
        u_hat = self.prop.eval(u)
        u_hat_sq = abs(u_hat)**2

        tmp = u_hat_sq / self.data_sq
        tmp[self.data_zeros] = 1
        u_hat_sq[self.data_zeros] = 0
        I_u_hat = tmp == 0
        tmp[I_u_hat] = 1
        tmp = log(tmp)
        h_u_hat = sum(u_hat_sq * tmp + self.data_sq - u_hat_sq).real
        # Now see that the propagated field is within the ball around the data (if any).
        # If not, the projection is calculated, otherwise we do nothing.
        if h_u_hat >= self.data_ball + self.TOL2:
            b = magproj(self.data, u_hat)
            return self.invprop.eval(b)
        else:
            return u


class Approx_P_M_masked(P_M_masked):
    def __init__(self, experiment):
        super(Approx_P_M_masked, self).__init__(experiment)
        self.data_sq = self.data ** 2
        self.data_zeros = where(self.data == 0)
        self.data_ball = experiment.data_ball
        self.TOL2 = experiment.TOL2

    def eval(self, u, prox_idx=None):
        u_hat = self.prop.eval(u)
        u_hat_sq = abs(u_hat) ** 2
        tmp = u_hat_sq / self.data_sq
        tmp[self.data_zeros] = 1
        u_hat_sq[self.data_zeros] = 0
        I_u_hat = tmp == 0
        tmp[I_u_hat] = 1
        tmp = log(tmp)
        h_u_hat = sum(u_hat_sq * tmp + self.data_sq - u_hat_sq).real
        # Now see that the propagated field is within the ball around the data (if any).
        # If not, the projection is calculated, otherwise we do nothing.
        if h_u_hat >= self.data_ball + self.TOL2:
            constrained = magproj(self.data, u_hat.copy())  # Apply constraint
            update = where(self.mask, u_hat, constrained)  # Masking operation
            return self.invprop(update)  # Propagate back
        else:
            return u

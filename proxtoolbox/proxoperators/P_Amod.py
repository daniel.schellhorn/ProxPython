
from proxtoolbox.proxoperators.proxoperator import ProxOperator
import numpy as np
from numpy import abs

class P_Amod(ProxOperator):
    """
    Projection onto support constraints
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on June 1, 2017.
    """

    def __init__(self, experiment):
        self.support_idx = experiment.support_idx

    def eval(self, u, prox_idx = None):
        """
        Projects the input data onto support constraints.

        Parameters
        ----------
        u : ndarray
            Function in the physical domain to be projected
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        p_A : ndarray
            The projection in the physical (time) domain.
            Leaves the phase alone on the support, resets
            amplitude to 1 and sets amplitude to 1 with 0
            phase outside support
        """
        p_A = np.ones_like(u)
        p_A[self.support_idx] = u[self.support_idx] / abs(u[self.support_idx])
        return p_A


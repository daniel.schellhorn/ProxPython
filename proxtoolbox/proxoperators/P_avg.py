
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.utils.cell import Cell, isCell
from numpy import zeros_like

class P_avg(ProxOperator):
    """
    Projection onto the diagonal, averaging the input

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Feb. 19, 2018.    
    """

    def __init__(self, experiment):
        self.n_prox = experiment.nProx
        self.Nx = experiment.Nx
        self.Ny = experiment.Ny
        self.Nz = experiment.Nz

    def eval(self, u, prox_index = None):
        """
        Averages the input u.  This is a projection onto onto the diagonal
        of a product space, but this routine dow not use that
        structure.  The only difference with P_Diag is that 
        the output is not copied onto the diagonal of the 
        implicit product spcace.

        Parameters
        ----------
        u : ndarray a list of ndarray objects
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_avg : ndarray or a list of ndarray objects
            The projection, the average.
        """
        if not isCell(u):
            k = self.n_prox # the number of prox mappings to average
            if self.Ny == 1:
                u_avg = sum(u,0)/k
            elif self.Nx == 1:
                u_avg = sum(u,1)/k
            elif self.Nz == 1:
                u_avg = u.mean(2)
            else:
                u_avg = u.mean(3)
        else:
            u_avg = zeros_like(u[0])
            N = len(u)
            for j in range(N):
                u_avg = u_avg + u[j]
            u_avg = u_avg / N

        return u_avg


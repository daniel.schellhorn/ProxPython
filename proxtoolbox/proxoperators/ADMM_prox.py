
from proxtoolbox import proxoperators
from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.accessors import accessors
import numpy as np
from numpy import pi, zeros, conj, log, real
from numpy.fft import fft2, ifft2, fft, ifft


class ADMM_Context:
    """
    Class containing data and helper functions for ADMM
    prox operators and ADMM related classes
    """
    def __init__(self, experiment):
        self.product_space_dimension = experiment.product_space_dimension
        # lambda may be set as well by the caller of eval method
        self.lmbda = experiment.lambda_0
        if hasattr(experiment, 'illumination'):
            self.illumination = experiment.illumination
        else:
            self.illumination = None
        if hasattr(experiment, 'FT_conv_kernel'):
            self.FT_conv_kernel = experiment.FT_conv_kernel
        else:
            self.FT_conv_kernel = None
        if hasattr(experiment, 'fresnel_nr'):
            self.fresnel_nr = experiment.fresnel_nr
        else:
            self.fresnel_nr = None
        self.farfield = experiment.farfield
        self.Nx = experiment.Nx
        self.Ny = experiment.Ny
        if hasattr(experiment, 'masks'):
            self.masks = experiment.masks
        else:
            self.masks = None
        if hasattr(experiment, 'magn'):
            self.magn = experiment.magn
        else:
            self.magn = None
        if hasattr(experiment, 'beam'):
            self.beam = experiment.beam
        else:
            self.beam = None



class Prox_primal_ADMM_indicator(ProxOperator, ADMM_Context):
    """
    Projecting onto physical (object) 
    domain constraints, adapted for the ADMM algorithm
    This is an approximate projector onto a ball around
    the data determined by the Kullback-Leibler divergence,
    as appropriate for Poisson noise.  The analysis of such 
    approximate projections was studied in 
    D.R.Luke, "Local Linear Convergence of Approximate 
    Projections onto Regularized Sets", Nonlinear Analysis, 
    75(2012):1531--1546.

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Oct 4, 2017.    
    """

    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADMM_Context.__init__(self, experiment)

        if isinstance(experiment.Prox0, str):
            proxClass = getattr(proxoperators, experiment.Prox0)
        else:
            proxClass = experiment.propagator
        self.Prox0 = proxClass(experiment)


    def eval(self, u, prox_index = None):
        """
        Projection subroutine for projecting onto physical (object) 
        domain constraints, adapted for the ADMM algorithm.
            
        Parameters
        ----------
        u : cell
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        xprime : cell
            The projection
        """
        m1, n1, p1, _q1 = size_matlab(u[0])
        k2 = self.product_space_dimension
        get, _set, _data_shape = accessors(u[1], self.Nx, self.Ny, k2)
           
        eta = self.lmbda

        xprime = np.zeros_like(u[0])

        if m1 > 1 and n1 > 1 and p1 == 1:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)
        elif m1 == 1 or n1 == 1:
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)
        else:
            raise NotImplementedError('Error Prox_primal_ADMM_indicator: phase reconstruction for 3D signals not implemented')

        for j in range(k2):
       
            if self.farfield:
                if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                        xprime += (self.Nx*self.Ny*2*pi) * IFFT(get(u[1],j) - get(u[2],j)/eta) / self.fresnel_nr[j]
                elif self.FT_conv_kernel is not None:
                        xprime += conj(self.FT_conv_kernel[j]) * IFFT(get(u[1],j) - get(u[2],j)/eta) * (self.Nx*self.Ny)
                elif self.masks is not None:
                        xprime += (self.Nx*self.Ny*2*pi) * IFFT(get(u[1],j) - get(u[2],j)/eta) / get(self.masks,j)
                else:
                        xprime += (self.Nx*self.Ny*2*pi) * IFFT(get(u[1],j) - get(u[2],j)/eta)
            else:  # near field
                if self.beam is not None:
                        xprime +=  IFFT(conj(self.FT_conv_kernel[j]) * FFT(get(u[1],j) - get(u[2],j)/eta) * get(self.magn,j)) / get(self.beam,j)

        # now project onto the constraints on the primal block
        xprime = self.Prox0.eval(xprime/k2)
        return xprime



class Approx_Pphase_FreFra_ADMM_Poisson(ProxOperator, ADMM_Context):
    """
    Prox operator  for computing the update to the auxilliary
    variables in the ADMM scheme subject to constraints in the 
    image domain (the intensity measurements in the 
    Fresnel/Fraunhofer domain.
    
    The projection onto the data constraints is an approximate
    projector onto a ball around the data determined
    by the Kullback-Leibler divergence, as appropriate
    for Poisson noise.  The analysis of such 
    approximate projections was studied in 
    D.R.Luke, "Local Linear Convergence of Approximate 
    Projections onto Regularized Sets", Nonlinear Analysis, 
    75(2012):1531--1546.

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Oct 4, 2017.
    """

    def __init__(self, experiment):
        ProxOperator.__init__(self, experiment)
        ADMM_Context.__init__(self, experiment)
        self.data = experiment.data
        self.data_sq = experiment.data_sq
        self.data_zeros = experiment.data_zeros
        self.data_ball = experiment.data_ball
        self.TOL2 = experiment.TOL2


    def eval_helper(self, u, j, proj_scale=1.0, proj_const=0.0):
        
        m1, n1, p1, _q1 = size_matlab(u)
        if p1 > 1:
            raise NotImplementedError('Error Approx_Pphase_FreFra_ADMM_Poisson: phase reconstruction for 3D signals not implemented')

        if m1 > 1 and n1 > 1:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)
        else:
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)
        if self.farfield:
            if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                u_hat = -1j*self.fresnel_nr[j]/(self.Nx*self.Ny*2*pi)*FFT(u-self.illumination[j]) + self.FT_conv_kernel[j]
            elif self.FT_conv_kernel is not None:
                u_hat = FFT(self.FT_conv_kernel[j]*u) / (self.Nx*self.Ny)
            else:
                u_hat = FFT(u) / (self.Nx*self.Ny)
        else: # near field
            if self.beam is not None:
                u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u*self.beam[j]))/self.magn[j]
            else:
                u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u))/self.magn[j]

        u_hat_sq = real(u_hat * conj(u_hat))
        # Update data_sq to prevent division by zero.
        # Note that data_sq_zeros, as defined below,
        # can be different from self.data_zeros[j]] 
        # (e.g., JWST experiment).
        data_sq = self.data_sq[j].copy()
        data_sq_zeros = np.where(data_sq == 0)
        data_sq[data_sq_zeros] = 1

        tmp = u_hat_sq / data_sq
        data_zeros = self.data_zeros[j]
        tmp[data_zeros] = 1
        u_hat_sq[data_zeros] = 0
        I_u_hat = tmp == 0
        tmp[I_u_hat] = 1
        tmp = log(tmp)
        h_u_hat = real(sum(sum(u_hat_sq*tmp + self.data_sq[j] - u_hat_sq)))
        if h_u_hat >= self.data_ball + self.TOL2:
            u_image = magproj(self.data[j], u_hat*proj_scale + proj_const)
        else:
            u_image = u
        return u_image


    def eval(self, u, prox_idx=None):
        """
        Projection onto the data constraints
            
        Parameters
        ----------
        u : cell
            Image in the physical domain to be projected.
            assumed here to be a cell, the first element of which
            project onto the amplitude constraints

        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_image : cell
            The projection in the same format as u.
        """
        if not isCell(u):
            u_image = self.eval_helper(u, 0)
        else:
            k2 = np.amax(u[2].shape)
            u_image = Cell(len(u[2]))
            eta = self.lmbda
            for j in range(k2):
                u_image[j] = self.eval_helper(u[0], j, eta, u[2][j])
        return u_image

